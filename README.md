## Instructions to Run this app
- Clone the repo

    ```git clone https://gitlab.com/konnectchetan/node-demo```
- Change directory

    ```cd node-demo```
- To build image

    ```docker build -t nodeapp .```
- Create Container
    
    ```docker run -d -p 8081:3333 nodeapp```
