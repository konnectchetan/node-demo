FROM node:14
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3333
CMD ["node", "app.js"]
